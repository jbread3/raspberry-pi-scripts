import os
from time import sleep
import signal
import sys
import RPi.GPIO as GPIO

pin = 18
maxTMP = 40

def setup():
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(pin, GPIO.OUT)
	GPIO.setwarnings(False)
	return()

def getCPUtemperature():
	res = os.popen('vcgencmd measure_temp').readline()
	temp = (res.replace("temp=", "").replace("'C\n", ""))
	return temp
def fanOn():
	setPin(True)
	return()
def fanOff():
	setPin(False)
	return()
def getTEMP():
	CPU_temp = float(getCPUtemperature())
	if CPU_temp > maxTMP:
		fanOn()
	else:
		fanOff()
	return()
def setPin(mode):
	GPIO.output(pin, mode)
	return()

try:
	setup()
	while True:
		getTEMP()
	sleep(5)
except KeyboardInterrupt:
	GPIO.cleanup()


