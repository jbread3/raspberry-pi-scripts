#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import subprocess


# This script can be disregarded. TheI believe using a device tree overlay
# is a better solution. (https://www.stderr.nl/Blog/Hardware/RaspberryPi/PowerButton.html)
# I reserve the right to take that statement back.
# > wget http://www.stderr.nl/static/files/Hardware/RaspberryPi/gpio-shutdown-overlay.dts
# > dtc -@ -I dts -O dtb -o gpio-shutdown.dtbo gpio-shutdown-overlay.dts
#   // feel free to ignore warnings about node fragment..
# > sudo cp gpio-shutdown.dtbo /boot/overlays/
# > dtoverlay=gpio-shutdown
#   // If `systemd --version` returns a version less than 225 run this:
# > ACTION!="REMOVE", SUBSYSTEM=="input", KERNEL=="event*", SUBSYSTEMS=="platform", DRIVERS=="gpio-keys", ATTRS{keys}=="116", TAG+="power-switch"

# this is a rip-off of commonly found script 
# not ideal due to polling 0.1 seconds
# code could use some cleaning
# Connect a momentary switch to pin5 and a ground pin
# save and the line to /etc/rc.local
#  sudo python /home/pi/scripts/shutdown.py &

GPIO.setmode(GPIO.BOARD)

GPIO.setup(5, GPIO.IN, pull_up_down = GPIO.PUD_UP)
oldButtonState1 = True

while True:
	buttonState1 = GPIO.input(5)

	if buttonState1 != oldButtonState1 and buttonState1 == False:
		subprocess.call("shutdown -h now", shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		oldButtonState1 = buttonState1
	
	time.sleep(0.1)
