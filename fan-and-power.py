#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import subprocess
import sys
import signal

resetPin = 5
fanPin = 12
oldButtonState1 = True

def setup():
	GPIO.setmode(GPIO.BOARD)
	GPIO.setup(resetPin, GPIO.IN, pull_up_down = GPIO.PUD_UP)
	GPIO.setup(fanPin, GPIO.OUT)
	
	# Fan is always on when pi is on.
	# look at fan-control.py to implement temp control
	GPIO.output(fanPin, True)	
	GPIO.setwarnings(False)
	return()

try:
	setup()
	while True:
		buttonState1 = GPIO.input(resetPin)

		if buttonState1 != oldButtonState1 and buttonState1 == False:
			subprocess.call("shutdown -h now", shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			oldButtonState1 = buttonState1
	
		time.sleep(1.0)
except KeyboardInterrupt:
	GPIO.cleanup()



