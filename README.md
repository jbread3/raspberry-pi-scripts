These are scripts that I use on some of my Raspberry Pis.
* fan-and-power.py 
  * This file will set GPIO pin 12(GPIO.BOARD) to high once and never take it down (turn fan on)
  * Then it enters a loop that is only exited on a keyboard interrupt.
  * In the loop it checks for a button press on GPIO pin 5.
  * It sleeps for 1 second (so you may have to hold the button for about a second)
  * The button is a typical momentary switch
* fan-control.py
  * This python script will send a GPIO pin high if the CPU temp meets a given threshold
  * The GPIO pin is then connected to the base pin on a NPN resistor, which controls a fan.
* power-button
  * This is the power button script, basically the power piece from fan-and-power.py
